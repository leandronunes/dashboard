class TableWidget < Widget

  before_create :load_data

  def load_data
    csv = ''
    csv << "DE501;15;1408:00;1311:29;874:29;533:31;37,89%"
    csv << "DE502;12;1112:00;1098:02;878:29;233:31;21,00%"
    csv << "DE501;15;1408:00;1311:29;874:29;533:31;37,89%"
    csv << "DE502;12;1112:00;1098:02;878:29;233:31;21,00%"
    csv << "DE501;15;1408:00;1311:29;874:29;533:31;37,89%"
    csv << "DE502;12;1112:00;1098:02;878:29;233:31;21,00%"
    table_content = csv 
  end

  def table_content
    self.settings(:data).table_content
  end

  def table_style
    self.settings(:data).table_style
  end

end
