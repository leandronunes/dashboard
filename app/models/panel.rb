class Panel < ActiveRecord::Base
  validates :name , presence: true
  has_many :widgets, :dependent => :destroy

  scope :active, lambda { where(:active => true) }
  scope :inactive, lambda { where(:active => false) }

  self.per_page = 5

end
