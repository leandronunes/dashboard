class Widget < ActiveRecord::Base
  validates_presence_of :name

  #FIXME validates this presence
  belongs_to :panel

  has_settings :data

  def self.widget_types
    [TableWidget,SonarWidget, AskbotWidget, UrlWidget]
  end

end
