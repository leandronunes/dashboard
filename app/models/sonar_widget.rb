class SonarWidget < Widget
  #FIXME make this test

  include HTTParty
  base_uri 'sonar.serpro/api'

  # Example of return
  # {"GED Build"=>0.0, "AFD"=>0.0, "ExtraTomDanado"=>20.0}
  def systems_coverage
    load_data if self.sonar_cache.nil? || self.expired?
    coverage_info = {}
    sonar_cache.map do |info|
      coverage_info[info['name']] = info['msr'].first['val']
    end
    
    coverage_info.reject!{|k| self.exclude_systems.include?(k)}
    coverage_info.sort_by{|k,v| -v}
  end

  def expired?
    return true if self.updated_at.nil?
    (self.updated_at > (DateTime.now - self.timeout))
  end

  def exclude_systems
    ['GED Build', 'AFD', 'ExtraTomDanado']
  end

  protected

  def timeout
    2.hours
  end

  def load_data
    self.sonar_cache = self.class.get("/resources?metrics=coverage&format=json")
  end

  def sonar_cache
    self.settings(:data).sonar_cache
  end

  def sonar_cache= sonar_cache
    self.settings(:data).sonar_cache = sonar_cache
  end
end
