class UrlWidget < Widget

  def self.permit
    [:name, :url]
  end

  def url
    self.settings(:data).url
  end

  def url= url
    self.settings(:data).url = url
  end

end
