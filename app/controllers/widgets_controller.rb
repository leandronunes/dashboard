class WidgetsController < ApplicationController
  before_action :set_panel
  before_action :set_widget, only: [:show, :edit, :update, :destroy]

  # GET /widgets
  # GET /widgets.json
  def index
    @widgets = @panel.widgets.paginate(:page => params[:page])
  end

  # GET /widgets/1
  # GET /widgets/1.json
  def show
  end

  # GET /widgets/new
  def new
    @widget = load_widget
  end

  # GET /widgets/1/edit
  def edit
  end

  # POST /widgets
  # POST /widgets.json
  def create
    @widget = load_widget(widget_params)
    @widget.panel = @panel

    respond_to do |format|
      if @widget.save
        format.html { redirect_to panel_widgets_path(@panel), notice: 'Widget was successfully created.' }
        format.json { render :show, status: :created, location: @widget }
      else
        format.html { render :new }
        format.json { render json: @widget.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /widgets/1
  # PATCH/PUT /widgets/1.json
  def update
    respond_to do |format|
      if @widget.update(widget_params)
        format.html { redirect_to panel_widgets_path(@panel), notice: 'Widget was successfully created.' }
        format.json { render :show, status: :ok, location: @widget }
      else
        format.html { render :edit }
        format.json { render json: @widget.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /widgets/1
  # DELETE /widgets/1.json
  def destroy
    @widget.destroy
    respond_to do |format|
      format.html { redirect_to panel_widgets_url(@panel), notice: 'Widget was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def widget_partial
    @widget = load_widget
    respond_to do |format|
      format.js
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_panel
      @panel = Panel.find(params[:panel_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_widget
      @widget = @panel.widgets.find(params[:id])
    end

    def load_widget(parameters = {})
      klass = Widget.descendants.detect{|w| w.name == params[:type]}
      @widget = (klass || UrlWidget).new(parameters)
      @widget
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def widget_params
      params.require(:widget).permit(UrlWidget.permit)
    end
end
