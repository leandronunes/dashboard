module ApplicationHelper

  def partial_for_widget(widget)
    widget.class.to_s.underscore
  end

  def widget_fullname(widget)
    t('widget_fullname', widget_name: widget.name, widget_type: widget.class.model_name.human)
  end

end
