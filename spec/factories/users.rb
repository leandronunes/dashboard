FactoryGirl.define do
  factory :user do
    login { FFaker::Lorem.words.to_s }
  end

end
